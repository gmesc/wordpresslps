<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', 'wplp_dev');

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define('DB_USER', 'root');

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define('DB_PASSWORD', '123456');

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', '127.0.0.1');

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '8P7p(9QH.BN!~T|qaj$L]v-uy`U-yf+Zs`c#@^:xfNV}bgkVQ6@q*=gevzsJHOC?');
 define('SECURE_AUTH_KEY',  'NZSY&w=1[)T|]xa|:Jlh9f&|,qUN6~yj8pO^|UWy5;#|`[a28B#_he1^nK701Das');
 define('LOGGED_IN_KEY',    'ggXQA/~H3JumV9~,J}c@6amX[S UMKM;+ #7TF$]<L#,3b6m v:o_IGCaMoHg}I_');
 define('NONCE_KEY',        'f~+TCG&LAh%6~;0 /XN0%]zYo2T@#H?^5%A83b-g04~?OM/y$x_+:~|A`h~g@{cZ');
 define('AUTH_SALT',        '+A+I{sUI-}l!_J<A7jL5LlqhkNZ8|eAAi.[L+.|}*=htZ^,TeVV%5v09Y!MfSbzc');
 define('SECURE_AUTH_SALT', 'jD(Y_0Z1{#u`Pfn=;`$`tl&z6D_#$1|p7_|R{hW@5F$#]&]Nj3s23:tH&B4hpSz@');
 define('LOGGED_IN_SALT',   '0}z Rv|(NS>V]:qIhlPudOnV?gwC+W}HBQJ2T4&5z@^!|&p Gq=a(<bhzS>@MFkK');
 define('NONCE_SALT',       '|.0oq>Fz-sB#Bt9dh;Qgfy#=?a^6{9B4kG;dkf<2QXXpY?|G e+@~~K$;MB lds+');
/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');
